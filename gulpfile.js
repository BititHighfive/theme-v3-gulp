var devsite = 'http://localhost/highfive2';

/*- Loading modules -*/

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var cssnano = require("cssnano");
var concat = require("gulp-concat");
var jshint = require('gulp-jshint');
var postcss = require('gulp-postcss');
var notify = require("gulp-notify");
var uglify = require('gulp-uglify');
var browserSync = require("browser-sync").create();

/*- Paths -*/

var paths = {
    styles: {
        src: 	'src/scss/base.scss',
        dest: 	'dist/css/',
        map: 	'map/'
    },
    theblocks: {
        src:    'src/scss/blocks/blocks.scss',
        dest:   'dist/editor/',
    },
    scripts: {
        src: 	'src/js/**/*.js',
        dest: 	'dist/js/',
    }
};

/*- Tasks -*/
 
function style() {
    var plugins = [
        autoprefixer(),
        cssnano()
    ];
 	return gulp
	  	.src(paths.styles.src)
	  	.pipe(sourcemaps.init())
	    .pipe(sass().on('error', sass.logError))
	    .pipe(postcss(plugins))
	    .pipe(sourcemaps.write(paths.styles.map))
	    .pipe(gulp.dest(paths.styles.dest))
        .pipe(browserSync.stream());
};

function myblocks() {
    return gulp
        .src(paths.theblocks.src)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.theblocks.dest))
        .pipe(browserSync.stream());
};

function script() {
	return gulp
		.src(paths.scripts.src)
		.pipe(sourcemaps.init())
		.pipe(concat('scripts.js'))
		.pipe(uglify())
    	.pipe(gulp.dest(paths.scripts.dest))
    	.pipe(browserSync.stream());
}

function lint() {
	return gulp
		.src(paths.scripts.src)
		.pipe(jshint())
    	.pipe(jshint.reporter('default'))
    	.pipe(jshint.reporter('fail'));
}

function reload() {
    browserSync.reload();
}

function watch() {
    browserSync.init({
        proxy: devsite
    });
    gulp.watch('src/scss/**/*.scss', style).on("change", reload);
    gulp.watch('src/scss/blocks/blocks.scss', myblocks).on("change", reload);
    gulp.watch(paths.scripts.src, script).on("change", reload);
}

exports.watch = watch;
exports.style = style;
exports.myblocks = myblocks;
exports.script = script;
exports.lint = lint;

var watcher = gulp.parallel(style, myblocks, script, lint, watch);

gulp.task('watch', watcher);