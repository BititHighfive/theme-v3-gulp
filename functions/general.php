<?php

/*
  Debug
 */
function debug($datas)
{
    echo '<pre style="background:black;color:white;text-align:left">';
      print_r($datas);
    echo '</pre>';
}

/*
  Disable Default Dashboard Widgets
*/

remove_action('welcome_panel', 'wp_welcome_panel');

function disable_default_dashboard_widgets() {
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
}
add_action('wp_dashboard_setup', 'disable_default_dashboard_widgets', 999);

/*
  Dasboard message
*/

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
 
function my_custom_dashboard_widgets() {
global $wp_meta_boxes;
 
wp_add_dashboard_widget('custom_help_widget', 'Bienvenue', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
echo "<p>Bienvenue sur votre espace d'administration.</p>";
}

/* 
  Hide admin notices 
*/

function pr_disable_admin_notices() {
    global $wp_filter;
      if ( is_user_admin() ) {
        if ( isset( $wp_filter['user_admin_notices'] ) ) {
                unset( $wp_filter['user_admin_notices'] );
        }
      } elseif ( isset( $wp_filter['admin_notices'] ) ) {
            unset( $wp_filter['admin_notices'] );
      }
      if ( isset( $wp_filter['all_admin_notices'] ) ) {
            unset( $wp_filter['all_admin_notices'] );
      }
  }
add_action( 'admin_print_scripts', 'pr_disable_admin_notices' );

/*
  Custom Backend Footer
*/

add_filter('admin_footer_text', 'thetheme_custom_admin_footer');
function thetheme_custom_admin_footer()
{
    echo '<span id="footer-thankyou">Developed by <a href="http://www.highfive.fr" target="_blank">Highfive</a></span>. Built using <a href="http://www.highfive.fr" target="_blank">The Theme</a>.';
}
add_filter('admin_footer_text', 'thetheme_custom_admin_footer');

/*
  SVG Medias
*/

function wpc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'wpc_mime_types');

/*
  Excerpt length
*/

function custom_excerpt_length( $length ) {
        return 15;
    }
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/*
  delete archive labels
*/

function highfive_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    } elseif ( is_year() ) {
        $title = get_the_date('Y');
    } elseif ( is_month() ) {
        $title = get_the_date('F Y');
    } elseif ( is_day() ) {
        $title = get_the_date('F j, Y');
    }
  
    return $title;
} 
add_filter( 'get_the_archive_title', 'highfive_archive_title' );

