<?php


function highfive_gutenberg_scripts() {

    wp_enqueue_script(
        'be-editor', 
        get_stylesheet_directory_uri() . '/src/js/editor-scripts.js', 
        array( 'wp-blocks', 'wp-dom' ), 
        filemtime( get_stylesheet_directory() . '/src/js/editor-scripts.js' ),
        true
    );
}
add_action( 'enqueue_block_editor_assets', 'highfive_gutenberg_scripts' );

/*
	MY BLOCKS
*/

add_action('acf/init', 'tablecontent_block');
function tablecontent_block() {

    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(array(
        'name'          => 'table-of-contents',
        'title'         => __( 'Table of Contents'),
        'description'       => 'Liste basée sur les titres H2',
        'render_template'   => 'page-templates/blocks/table-of-content.php',
        'category'      => 'highfive-blocks',
        'icon'          => 'list-view',
            'supports' => array(
                'align' => false,
            ),
        ));
    }
}

/*
	GUTENBERG ACTIVE PAGES
*/

// add_filter( 'allowed_block_types', 'highfive_allowed_block_types', 10, 2 );
 
// function highfive_allowed_block_types( $allowed_blocks, $post ) {

// 	$templatesarray = get_pages(array(
// 	    'meta_key' => '_wp_page_template',
// 	    /* edit templates */
// 	    'meta_value' => 'page-templates/tpl-editorial_classic.php'
// 	));

// 	$templatearrayids = array();
// 	foreach($templatesarray as $pagearray){
// 		$templatearrayids[] = $pagearray->ID;
// 	}

// 	if( $post->post_type === 'page' ) {
// 		$allowed_blocks = array('');
// 	}

// 	if( in_array( $post->ID, $templatearrayids)) {
// 		$allowed_blocks = array(
// 			'core/paragraph',
// 			'core/image',
// 			'core/heading',
// 			'core/file',
// 			'acf/auteur',
//             'acf/tarifs',
//             'acf/pros',
//             'acf/table-of-contents',
//             'acf/profiles',
// 			'core/list',
//             'core/gallery',
//             'core/freeform',
//             'core/columns',
//             'core/media-text',
//             'acf/boutonswide',
//             'acf/boutonicone',
//             'acf/presentation',
//             'core/spacer',
//             'core/separator',
//             'core/shortcode',
//             'core/button',
//             'acf/partenaires'
// 		);
// 	}
 
// 	return $allowed_blocks; 
// }

/*----------- GUTENBERG Categories --------------*/

function highfive_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'highfive-blocks',
                'title' => __( 'Custom Blocks', 'highfive' ),
                'icon'  => 'flag',
            ),
        )
    );
}
add_filter( 'block_categories', 'highfive_block_categories', 10, 2 );

/**-------- GUTENBERG Styles -----------**/

function mytheme_setup_theme_supported_features() {
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => 'bleu',
            'slug' => 'bleu-normal',
            'color' => '#002d60',
        ),
        array(
            'name' => 'bleu foncé',
            'slug' => 'bleu-fonce',
            'color' => '#00244d',
        ),
        array(
            'name' => 'doré',
            'slug' => 'gold-classic',
            'color' => '#cba43d',
        ),
        array(
            'name' => 'doré clair',
            'slug' => 'gold-clear',
            'color' => '#ffd157',
        ),
        array(
            'name' => 'blanc',
            'slug' => 'white-base',
            'color' => '#FFF',
        ),
        array(
            'name' => 'Noir',
            'slug' => 'noir-base',
            'color' => '#333',
        ),
    ) );

    add_theme_support( 'editor-font-sizes', array(
	    array(
	        'name' => 'Petit',
	        'size' => 14,
	        'slug' => 'small'
	    ),
	    array(
	        'name' => 'Normal',
	        'size' => 16,
	        'slug' => 'normal'
	    ),
	    array(
	        'name' => 'Sous titres',
	        'size' => 25,
	        'slug' => 'stitre'
	    ),
	    array(
	        'name' => 'Titres',
	        'size' => 35,
	        'slug' => 'huge'
	    )
	) );
}
 
add_action( 'after_setup_theme', 'mytheme_setup_theme_supported_features' );
