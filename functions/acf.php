<?php
/*
* Page d'options ACF
*/
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();    
}

/**
 * Editeur WYSIWYG
 * https://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/
 */
function my_toolbars($toolbars)
{
    // Add a new toolbar called "Very Simple"
    // - this toolbar has only 1 row of buttons
    $toolbars['Very Simple'] = array();
    $toolbars['Very Simple'][1] = array('bold', 'italic', 'underline', 'bullist', 'numlist', 'undo', 'redo',
        'link', 'unlink', 'fullscreen');

    //Add a new toolbar called "Custom Full"
    // - this toolbar has only 1 row of button
    $toolbars['Custom Full'] = array();
    $toolbars['Custom Full'][1] = array('formatselect', 'bold', 'italic', 'underline', 'bullist', 'numlist', 'undo', 'redo',
        'link', 'unlink', 'fullscreen', 'pastetext', 'removeformat', 'charmap');

    // return $toolbars - IMPORTANT!
    return $toolbars;
}
add_filter('acf/fields/wysiwyg/toolbars', 'my_toolbars');