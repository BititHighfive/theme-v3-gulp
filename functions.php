<?php

/*
    Thumbnails sizes
*/

add_image_size( 'img-maxsize', 1500, 999999, true );

/*
    Enqueue CSS styles.
*/

if (!function_exists("thetheme_theme_styles")) {
    function thetheme_theme_styles()
    {
        wp_register_style('thetheme-main', get_template_directory_uri() . '/dist/css/base.css', '1.0', 'all');
        wp_enqueue_style('thetheme-main');
    }
}
add_action('wp_enqueue_scripts', 'thetheme_theme_styles');

/*
    Enqueue jQuery / Javascript
*/

if (!function_exists("thetheme_theme_js")) {
    function thetheme_theme_js()
    {
        wp_register_script('thetheme-js', get_template_directory_uri() . '/dist/js/scripts.js', array('jquery') , null , 'all');
        wp_enqueue_script('thetheme-js');
    }
}
add_action('wp_enqueue_scripts', 'thetheme_theme_js');

/**************************************************/
/***************** MANAGE SIDEBARS ****************/
/**************************************************/

// Sidebars & Widgetizes Areas

function thetheme_register_sidebars()
{
    register_sidebar(array(
        'id' => 'sidebar1',
        'name' => 'Main Sidebar',
        'description' => 'Classic sidebar.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'id' => 'footer',
        'name' => 'Footer',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
}
add_action('widgets_init', 'thetheme_register_sidebars');


/*------------------------------------------------*/
/*------------ TOOLS / TIPS & TRICKS -------------*/
/*------------------------------------------------*/

// Wordpress Head Cleanup

if (!function_exists("thetheme_head_cleanup")) {
    function thetheme_head_cleanup()
    {
        remove_action('wp_head', 'feed_links_extra', 3);                   
        remove_action('wp_head', 'feed_links', 2);                          
        remove_action('wp_head', 'rsd_link');                               
        remove_action('wp_head', 'wlwmanifest_link');                       
        remove_action('wp_head', 'index_rel_link');                         
        remove_action('wp_head', 'parent_post_rel_link', 10, 0);            
        remove_action('wp_head', 'start_post_rel_link', 10, 0);             
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); 
        remove_action('wp_head', 'wp_generator'); 
        remove_action('wp_head', 'print_emoji_detection_script', 7);                          
    }
}
add_action('init', 'thetheme_head_cleanup');

remove_action('wp_print_styles', 'print_emoji_styles');
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// Remove WP version

function thetheme_rss_version() {
        return '';
    }
add_filter('the_generator', 'thetheme_rss_version');

function wpb_remove_version() {
        return '';
    }
add_filter('the_generator', 'wpb_remove_version');

// block login by email

remove_filter( 'authenticate', 'wp_authenticate_email_password', 20 );

// Add WP Functions & Theme Support

if (!function_exists("thetheme_theme_support")) {
    function thetheme_theme_support()
    {
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');        
        add_theme_support('automatic-feed-links');
        add_theme_support('customize-selective-refresh-widgets');
        add_theme_support('menus');
        add_theme_support(
            'html5',
            array(
                'search-form',
                'gallery',
                'caption',
            )
        );
        add_theme_support(
            'custom-logo',
            array(
                'height'      => 250,
                'width'       => 500,
                'flex-height' => true,
                'flex-width'  => true,
                'header-text' => array( 'site-title', 'site-description' ),
            )
        );        
        add_theme_support('align-wide');
        add_theme_support('disable-custom-colors');
        add_theme_support('disable-custom-font-sizes');
        add_theme_support('editor-styles');    
        add_theme_support('wp-block-styles');
        add_theme_support('responsive-embeds');
        
        add_editor_style( get_template_directory_uri(). '/dist/editor/blocks.css');

        register_nav_menus(
            array(
                'main_nav' => 'The Main Menu',   // main nav in header
                'footer_links' => 'Footer Links' // secondary nav in footer
            )
        );
    }
}
add_action('after_setup_theme', 'thetheme_theme_support');

// Main nav menu

function thetheme_main_nav()
{
    wp_nav_menu(
        array(
            'menu' => 'main_nav',
            'menu_class' => 'nav',
            'theme_location' => 'main_nav',
            'container' => 'false',
        )
    );
}

// Footer nav menu

function thetheme_footer_links()
{
    wp_nav_menu(
        array(
            'menu' => 'footer_links',
            'theme_location' => 'footer_links',
            'container_class' => 'footer-links',
        )
    );
}

/*
 HIGHFIVE :
 */
include_once 'functions/general.php';
include_once 'functions/acf.php';
include_once 'functions/gutenberg.php';

?>
