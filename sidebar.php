<div id="sidebar1" role="complementary">

	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar1' ); ?>

	<?php else : ?>
	
		<div class="alert alert-message">
		
			<p><?php _e("Please activate some Widgets","wpbootstrap"); ?>.</p>
		
		</div>

	<?php endif; ?>

</div>